<?php

require_once $templatePath . '/libraries/scssphp/scss.inc.php';

use ScssPhp\ScssPhp\Compiler;

try {
    $scss = new Compiler();

    // Set filepath to compile
    $content = file_get_contents($templatePath . '/scss/template.scss');

    // Set import paths
    $scss->setImportPaths($templatePath . '/scss/', $templatePath . '/scss/bootstrap/');

    // compile content
    $compiledContent = $scss->compile($content);

    // Write content to CSS file
    file_put_contents($templatePath . '/css/template.css', $compiledContent );
} catch (\Exception $e) {
    echo '';
    syslog(LOG_ERR, 'scssphp: Unable to compile content');
}
