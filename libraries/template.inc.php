<?php
    defined( '_JEXEC' ) or die;

    /**
     * Joomla Blank Template - Library v1.0.0
     * https://gitlab.com/LittleToni
     * Copyright 2019, Toni Patke <toni.patke@outlook.de>
     */

    $app = JFactory::getApplication();
    $doc = JFactory::getDocument();
    $menu = $app->getMenu();

    $templatePath = $this->baseurl . 'templates/' . $this->template;
    $rootPath = $_SERVER['DOCUMENT_ROOT'] . '/templates/' . $this->template;

    /**
     * Set default body css-class. 
     * Extend class if active page is default page.
     */
    $bodyClass = 'body';

    if ($menu->getActive() == $menu->getDefault()) {
        $bodyClass .= ' is-frontpage';
    }

    /**
     * Get used language and language direction.
     */
    $lang = JFactory::getLanguage();
    $direction = $lang->get('rtl');

    if ($direction == 0) {
        $direction = 'ltr';
    } else {
        $direction = 'rtl';
    }

    /**
     * Unset Joomla default styles
     * Unset Joomla default Scripts
     */

    foreach ($doc->_styleSheets as $sheet => $settings) {
        unset($doc->_styleSheets[$sheet]);
    }

    foreach ($doc->_scripts as $script => $settings) {
        unset($doc->_scripts[$script]);
    }

    // ToDo : Delete script & Style Declarations

    /**
     * Using Pre-Processor SASS (https://sass-lang.com/) to extend bootstrap 4 scss.
     * To check for compile with scssphp by leafo (https://scssphp.github.io/scssphp/)
     * compare unix timestamps of template.css and all scss files.
     */
    $cssPath = $templatePath . '/css/template.css';
    $scssDir = $templatePath . '/scss/';
    $compile = false;

    function getDirContents($dir, &$results = array()) {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir.DIRECTORY_SEPARATOR.$value);

            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != '.' && $value != '..') {
                getDirContents($path, $results);
            }
        }

        return $results;
    }

    $scssFiles = getDirContents($scssDir);

    foreach ($scssFiles as $scssFile) {
        if (filemtime($scssFile) > filemtime($cssPath)) {
            $compile = true;
            break;
        }
    }

    if ($compile == true) {
        require __DIR__ . '/template.compile.php';
    }
