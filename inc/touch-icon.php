<?php defined('_JEXEC') or die; ?>

<link rel="apple-touch-icon-precomposed" href="<?php echo $templatePath; ?>/images/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $templatePath; ?>/images/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $templatePath; ?>/images/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $templatePath; ?>/images/apple-touch-icon-144x144-precomposed.png">