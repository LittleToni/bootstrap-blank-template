<?php
    defined('_JEXEC') or die;

    include_once JPATH_THEMES . '/' . $this->template . '/libraries/template.inc.php';
?>

<!DOCTYPE html>

<html dir="<?php echo $direction; ?>" lang="<?php echo $this->language; ?>">
    <head>
        <jdoc:include type="head" />

        <?php include_once 'inc/meta-data.php'; ?>
        <?php include_once 'inc/touch-icon.php'; ?>

        <link rel="stylesheet" href="<?php echo $templatePath; ?>/css/template.css">
    </head>

    <body class="<?php echo $bodyClass; ?>">
        <jdoc:include type="modules" name="fixed" />
        <jdoc:include type="modules" name="top" />

        <main>
            <jdoc:include type="message" />
            <jdoc:include type="component" />
        </main>

        <jdoc:include type="modules" name="bottom" />

        <!-- Bootstrap 4, jQuery 3.2.1, popper.js & template.js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="<?php echo $templatePath; ?>/js/template.js"></script>
    </body>
</html>
